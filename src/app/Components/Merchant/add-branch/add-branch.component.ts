import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataService } from '../../../Services/data.service';
import { MessageService } from '../../../Services/message.service';
import { Constants } from '../../../Utilities/constants';
import { Sessions } from '../../../Utilities/session';

@Component({
  selector: 'app-add-branch',
  templateUrl: './add-branch.component.html',
  styleUrls: ['./add-branch.component.css']
})
export class AddBranchComponent implements OnInit {

  merchantStoreId;

  constructor(public dataService: DataService, public messageService: MessageService,public router: Router) { }
  
  ngOnInit() {
    this.merchantStoreId = Sessions.getLocal(Constants.localStorageObjects.merchantStoreId);
    this.fetchMerchantInfromation();
  }

  submitted: boolean;
  messageObj: {};

  branchForm = new FormGroup({
    branchtitle: new FormControl('', Validators.required),
    branchManagerFullName: new FormControl('', Validators.required),
    branchManagerEmail: new FormControl('', Validators.required),
    contactNo: new FormControl('', Validators.required),
    address1: new FormControl('', Validators.required),
    address2:  new FormControl('', Validators.required),
    country: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    province: new FormControl('', Validators.required),
    zipCode: new FormControl('', Validators.required),
    security: new FormControl('', Validators.required),

  });
  // formControls check
  formControls = this.branchForm.controls;

  storeId;
  async next() {
    this.submitted = true;

    if (this.branchForm.valid) {
      let createBranchObj = {
        store: this.merchantStoreId,
        branchTitle: this.branchForm.value.branchtitle,
        branchManager: this.branchForm.value.branchManagerFullName,
        email: this.branchForm.value.branchManagerEmail,
        contactNo: this.branchForm.value.contactNo,
        address1: this.branchForm.value.address1,        
        address2: this.branchForm.value.address2,
        country: this.branchForm.value.country,
        city: this.branchForm.value.city,
        province: this.branchForm.value.province,
        
      };

      let response = await this.dataService.createBranch(createBranchObj);

      if (response.metadata.status === 'SUCCESS') {

        Sessions.createLocal( Constants.localStorageObjects.storeId, response.payload.store.storeId);

        this.messageObj = {
          message: response.metadata.message,
          color: Constants.displayColorCode.color
        };

        this.messageService.sendMessage(this.messageObj);

        setTimeout(() => { 
          this.router.navigate(['merchant-information']);
        }, 3000); 

      } else {
        // if not added then
        this.messageObj = {
          message: response.metadata.message,
          color: Constants.displayColorCode.color
        };

        this.messageService.sendMessage(this.messageObj);
      }
      this.submitted = false;
      this.branchForm.reset();
    }
  }

  async fetchMerchantInfromation(){
    
    this.storeId = Sessions.getLocal(Constants.localStorageObjects.storeId)
    let storeObj = {
      store :   this.storeId 
    }

    let response = await this.dataService.fetchMerchantInformation(storeObj);

    if (response.metadata.status == "SUCCESS") {
      Sessions.createLocal( Constants.localStorageObjects.merchantInformation, response.payload.store);
      Sessions.createLocal( Constants.localStorageObjects.merchantId, response.payload.store.storeId);
      Sessions.createLocal( Constants.localStorageObjects.merchantBranches, response.payload.store.branches);
    }
  }
}