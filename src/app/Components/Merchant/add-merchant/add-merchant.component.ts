import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataService } from '../../../Services/data.service';
import { MessageService } from '../../../Services/message.service'
import { Constants } from '../../../Utilities/constants';
import { Sessions } from '../../../Utilities/session';

@Component({
  selector: 'app-add-merchant',
  templateUrl: './add-merchant.component.html',
  styleUrls: ['./add-merchant.component.css']
})
export class AddMerchantComponent implements OnInit {

  constructor(public dataService: DataService, public messageService: MessageService,public router: Router) { }

  ngOnInit() {
  }

  submitted: boolean;
  messageObj: {};
  storeId;

  merchantForm = new FormGroup({
    title: new FormControl('', Validators.required),
    businessOwnerFullName: new FormControl('', Validators.required),
    businessOwnerEmail: new FormControl('', Validators.required),
    businessOwnerContactNo: new FormControl('', Validators.required),
    type: new FormControl('', Validators.required),
    security: new FormControl('', Validators.required),
    subscriptionType: new FormControl('', Validators.required),
    subscriptionPlan: new FormControl('', Validators.required),

  });
  // formControls check
  formControls = this.merchantForm.controls;

  async createMerchant() {

    this.submitted = true;

    if (this.merchantForm.valid) {

      let createMerchantObj = {
        title: this.merchantForm.value.title,
        businessOwnerFullName: this.merchantForm.value.businessOwnerFullName,
        businessOwnerEmail: this.merchantForm.value.businessOwnerEmail,
        businessOwnerContactNo: this.merchantForm.value.businessOwnerContactNo,
        type: this.merchantForm.value.type,
        subscription: {
          security: this.merchantForm.value.security,
          type: this.merchantForm.value.subscriptionType,
          plan: this.merchantForm.value.subscriptionPlan,
        }

      };
      let response = await this.dataService.createMerchant(createMerchantObj);

      if (response.metadata.status === 'SUCCESS') {

        Sessions.createLocal(Constants.localStorageObjects.merchantStoreId, response.payload.storeId);

        this.messageObj = {
          message: response.metadata.message,
          color: Constants.displayColorCode.color
        };

        this.messageService.sendMessage(this.messageObj);

        setTimeout(() => { 
          this.router.navigate(['addBranch']);
        }, 3000); 

      } else {
        // merchant not added
        this.messageObj = {
          message: response.metadata.message,
          color: Constants.displayColorCode.color
        };

        this.messageService.sendMessage(this.messageObj);

      }
      this.submitted = false;
      //this.router.navigate(['addBranch']); 
      this.merchantForm.reset();
    }
 
  }
}