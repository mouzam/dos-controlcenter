import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Sessions } from '../../../Utilities/session';
import { Constants } from '../../../Utilities/constants';
import { DataService } from '../../../Services/data.service'

@Component({
  selector: 'app-merchant-details',
  templateUrl: './merchant-details.component.html',
  styleUrls: ['./merchant-details.component.css']
})
export class MerchantDetailsComponent implements OnInit {

  constructor(private dataService: DataService,public router: Router) { }

  ngOnInit() {

    this.fetchMerchants();
  }

  merchantDetails;
  merchantObj;

  async fetchMerchants() {
    let response = await this.dataService.fetchMerchantDetails();

    if (response.metadata.status === 'SUCCESS') {

      this.fetchMerchants = response.payload.list;

      Sessions.createLocal(Constants.localStorageObjects.fetchMerchantDetails, response.payload.list);
      this.merchantObj = Sessions.getLocal(Constants.localStorageObjects.fetchMerchantDetails);

    }
    else {
      // exception
    }

  }
  addMerchant(){
      this.router.navigate(['addMerchant']);
  }

  async open(id){
  
    let storeObj = {
      store :   id // to be removed
    }

    console.log(storeObj);

    let response = await this.dataService.fetchMerchantInformation(storeObj);
  
    if (response.metadata.status == "SUCCESS") {
      Sessions.createLocal( Constants.localStorageObjects.merchantInformation, response.payload.store);
      Sessions.createLocal( Constants.localStorageObjects.merchantId, response.payload.store.storeId);
      Sessions.createLocal( Constants.localStorageObjects.merchantBranches, response.payload.store.branches);
    }

    this.router.navigate(['merchant-information']);  // navigate
  }
}