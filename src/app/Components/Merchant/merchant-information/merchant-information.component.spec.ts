import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MerchantInformationComponent } from './merchant-information.component';

describe('MerchantInformationComponent', () => {
  let component: MerchantInformationComponent;
  let fixture: ComponentFixture<MerchantInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MerchantInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MerchantInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
