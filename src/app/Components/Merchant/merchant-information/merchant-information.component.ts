import { Component, OnInit } from '@angular/core';
import {Sessions} from '../../../Utilities/session';
import {Constants} from '../../../Utilities/constants';
import { DataService } from '../../../Services/data.service';
import { MessageService } from '../../../Services/message.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Data } from '@angular/router';

@Component({
  selector: 'app-merchant-information',
  templateUrl: './merchant-information.component.html',
  styleUrls: ['./merchant-information.component.css']
})
export class MerchantInformationComponent implements OnInit {

  constructor(public modalService: NgbModal, public dataService : DataService, public messageService : MessageService) { }

  messageObj = {};
  detailsObj= {};

  branches ;
  offerId; 
  submitted : boolean;
  closeResult: string;
  merchantStoreId;
  
  ngOnInit() {

    this.detailsObj = Sessions.getLocal(Constants.localStorageObjects.merchantInformation);
    
    this.branches = Sessions.getLocal(Constants.localStorageObjects.merchantBranches);
      
    this.merchantStoreId = Sessions.getLocal(Constants.localStorageObjects.merchantId);
  
  }

  branchForm = new FormGroup({
    branchtitle: new FormControl('', Validators.required),
    branchManagerFullName: new FormControl('', Validators.required),
    branchManagerEmail: new FormControl('', Validators.required),
    contactNo: new FormControl('', Validators.required),
    address1: new FormControl('', Validators.required),
    address2:  new FormControl('', Validators.required),
    country: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    province: new FormControl('', Validators.required),
    zipCode: new FormControl('', Validators.required),
    security: new FormControl('', Validators.required),

  });
  // formControls check
  formControls = this.branchForm.controls;

  open(content) {

    this.modalService.open(content, {
       ariaLabelledBy: 'modal-basic-title',

      }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
    
  async addBranch() {

    this.submitted = true;
    
    if (this.branchForm.valid) {
      let createBranchObj = {
        store: this.merchantStoreId,
        branchTitle: this.branchForm.value.branchtitle,
        branchManager: this.branchForm.value.branchManagerFullName,
        email: this.branchForm.value.branchManagerEmail,
        contactNo: this.branchForm.value.contactNo,
        address1: this.branchForm.value.address1,        
        address2: this.branchForm.value.address2,
        country: this.branchForm.value.country,
        city: this.branchForm.value.city,
        province: this.branchForm.value.province,
        
      };

      let response = await this.dataService.createBranch(createBranchObj);
  
      if (response.metadata.status === 'SUCCESS') {

        this.messageObj = {
          message: response.metadata.message,
          color: Constants.displayColorCode.color
        };

        this.messageService.sendMessage(this.messageObj);

      } else {
        //  not added
        this.messageObj = {
          message: response.metadata.message,
          color: Constants.displayColorCode.color
        };

        this.modalService.dismissAll();       // check
        this.messageService.sendMessage(this.messageObj);
      }
      
      this.submitted = false;
      this.branchForm.reset();
  }
 }

  toggleAccordian(event, index) {
    var element = event.target;
    element.classList.toggle("active");
    if(this.branches[index].isActive) {
      this.branches[index].isActive = false;
    } else {
      this.branches[index].isActive = true;
    }      
    var panel = element.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  }
}