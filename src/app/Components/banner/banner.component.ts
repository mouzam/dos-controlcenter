import { Component, OnInit } from '@angular/core';

import { MessageService} from "../../Services/message.service";

import { Subscription } from 'rxjs';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {

  showMessage;
  colors;
  showSuccessMessage : boolean;
  subscription: Subscription;

  constructor(private messageService : MessageService) {
    this.showSuccessMessage = true;
   }

  ngOnInit() {

    this.subscription = this.messageService.getMessage().subscribe(message => {
    
      if (message) {
        this.showMessage = message.display.message;
        this.colors = message.display.color;
        setTimeout(() => (this.showMessage = ""), 3000);

      } else {
        // clear messages when empty message received
        this.showMessage = '';
      }
    });
  }
  clearMessages(): void {
    // clear messages
    this.messageService.clearMessages();

  }
}
