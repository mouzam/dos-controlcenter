import { Component, OnInit } from '@angular/core';
import { MessageService} from "../../Services/message.service";
import {DataService} from '../../Services/data.service'
import {Constants} from '../../Utilities/constants';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  
  messageObj = {}
  usersArray=[];

  constructor(private messageService : MessageService, private dataService:DataService) { }

  ngOnInit() {
  // this.getallUSers();
  }

  async getallUSers() 
  {
    let response = await this.dataService.getAllUsers();

    for (var index = 0; index < response.length; index++) {

      this.usersArray[index] = response[index];

    }
    console.log(this.usersArray);
  }
  submit ()
  { 
    this.messageObj = {
      message :Constants.ResponseMessages.submitMessage,
      color : Constants.displayColorCode.color          
    }
    this.messageService.sendMessage(this.messageObj);

  }
}
