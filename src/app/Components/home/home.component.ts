import { Component, OnInit , Input, Output, EventEmitter} from '@angular/core';
import {Sessions} from '../../Utilities/session';
import {Constants} from '../../Utilities/constants';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [
    CommonModule
  ]
})

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  
})
export class HomeComponent implements OnInit {

  constructor() { }

  merchantObj = {};
  branchObj = {};
  branchArray =[];
  panelExpanded = false;
  isHidden = false;

  ngOnInit() {

    this.merchantObj = Sessions.getLocal(Constants.localStorageObjects.merchantInfo);

    this.branchObj =  Sessions.getLocal(Constants.localStorageObjects.branches);

  }

  toggleAccordian(event, index) {
    var element = event.target;
    element.classList.toggle("active");
    if(this.branchObj[index].isActive) {
      this.branchObj[index].isActive = false;
    } else {
      this.branchObj[index].isActive = true;
    }      
    var panel = element.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  }
}
