import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { NgForm } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService} from '../../Services/message.service';
import {DataService} from '../../Services/data.service';
import { AuthService } from 'src/app/Services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  isSubmitted  =  false;
  messageObj = {};
  constructor( private router: Router, public auth: AuthService, private formBuilder: FormBuilder,private dataService: DataService) { }

  async ngOnInit() {

     // Build Form on Component Initialization
     this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

   // On Login Method
   login() {
    this.isSubmitted = true;

    if ( this.loginForm.invalid) {
      return;
    } else if (this.loginForm.valid) {
      this.isSubmitted = false;

      this.auth.login(this.loginForm.value.email , this.loginForm.value.password);
      this.loginForm.reset();
    }
  }
}
