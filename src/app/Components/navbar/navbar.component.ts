import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import {AuthService} from '../../Services/auth.service';
import {Sessions} from '../../Utilities/session';
import {Constants} from '../../Utilities/constants';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private router: Router, private auth: AuthService) { }
  title = 'DOS';
  login;
  navbarOpen = false;

  ngOnInit() {

    this.login =  Sessions.getLocal( Constants.localStorageObjects.merchantInfo);
    
   console.log( 'administration =' , Sessions.getLocal( Constants.localStorageObjects.merchantInfo));
  }

  
  openNav() {
    this.navbarOpen = !this.navbarOpen;
    if(this.navbarOpen){
     document.getElementById("multiCollapseExample").style.width = "250px";
 
    }else {
      
    document.getElementById("multiCollapseExample").style.width = "60px";
    }
  }

  logout() {
    this.auth.logout();
  }
}
