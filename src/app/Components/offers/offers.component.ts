import { Component, OnInit } from '@angular/core';
import { Sessions } from '../../Utilities/session';
import { Constants } from '../../Utilities/constants';
import { DataService } from '../../Services/data.service';
import { MessageService } from '../../Services/message.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [
    CommonModule
  ]
})

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.css']
})
export class OffersComponent implements OnInit {

  merchantObj = {};
  storeId;
  offerId;
  offers;
  submitted: boolean;
  isSubmitted: boolean;
  closeResult: string;

  offerForm = new FormGroup({
    title: new FormControl('', Validators.required),
    price: new FormControl('', Validators.required),
    mode: new FormControl('', Validators.required),
    branch: new FormControl('', Validators.required),

  });
  // formControls check
  formControls = this.offerForm.controls;
  messageObj: { message: string; color: string; };

  constructor(public dataService: DataService, public modalService: NgbModal, public messageService: MessageService) { }

  ngOnInit() {

    this.merchantObj = Sessions.getLocal(Constants.localStorageObjects.merchantInfo);

    console.log('merchant obj',Sessions.getLocal(Constants.localStorageObjects.merchantInfo))
    this.storeId = Sessions.getLocal(Constants.localStorageObjects.storeId);

    this.fetchOffers();
  }


  open(content) {

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openEditOffers(editContent, event) {
    this.offerId= event.currentTarget.getAttribute('id');
    this.modalService.open(editContent, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  async fetchOffers() {

    let fetchOfferObj = {
      store: this.storeId,
      pageSize: 500,
      offset: 0
    };
  
    let response = await this.dataService.fetchOffers(fetchOfferObj);
    console.log('fetch offers response =', response);

    if (response.metadata.status === 'SUCCESS') {
      this.offers = response.payload.offers;
    }
  }

  async createOffer() {

    this.submitted = true;
  
    if (this.offerForm.valid) {

      let createOffersObj = {
        title: this.offerForm.value.title,
        price: this.offerForm.value.price,
        mode: this.offerForm.value.mode,
        branch: this.offerForm.value.branch,

        pageSize: Constants.defaults.pageSize,
        offset: Constants.defaults.offset,
        store: this.storeId
      };

      let response = await this.dataService.createOffers(createOffersObj);
      console.log('create offers response =', response);

      if (response.metadata.status === 'SUCCESS') {

        this.offerId = response.payload.offers[0].offerId;
        
        this.messageObj = {
          message: response.metadata.message,
          color: Constants.displayColorCode.color
        };

        this.modalService.dismissAll();
        this.messageService.sendMessage(this.messageObj);
      }

    }
    this.offerForm.reset();
  }

 async updateOffer() {
    this.isSubmitted = true;
    if (this.offerForm.valid) {

      let updateOffersObj = {
        offer: this.offerId,
       // isActive: 'Active',
        title: this.offerForm.value.title,
        price: this.offerForm.value.price,
        mode:  this.offerForm.value.mode,
        branch: this.offerForm.value.branch,

        pageSize: Constants.defaults.pageSize,
        offset: Constants.defaults.offset,
      };
      
      let response = await this.dataService.updateOffers(updateOffersObj);
     

      if (response.metadata.status === 'SUCCESS') { 

        this.messageObj = {
          message: response.metadata.message,
          color: Constants.displayColorCode.color
        };
        this.modalService.dismissAll();
        this.messageService.sendMessage(this.messageObj);
      }else {
        this.messageObj = {
          message: 'Offer not updated..!!',
          color: Constants.displayColorCode.color
        };
        this.modalService.dismissAll();
        this.messageService.sendMessage(this.messageObj);
      }
      this.offerForm.reset();
    }
  }
}
