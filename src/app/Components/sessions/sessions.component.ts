import { Component, OnInit } from '@angular/core';
import { Sessions } from '../../Utilities/session';
import { Constants } from '../../Utilities/constants';
import { DataService } from '../../Services/data.service';

@Component({
  selector: 'app-sessions',
  templateUrl: './sessions.component.html',
  styleUrls: ['./sessions.component.css']
})
export class SessionsComponent implements OnInit {

  storeId;
  sessions;
  constructor(public dataService: DataService) { }

  ngOnInit() {
    
    this.storeId = Sessions.getLocal(Constants.localStorageObjects.storeId);
    this.fetchSessions();
  }

  
  async fetchSessions() {

    let fetchSessionsObj = {
      store: this.storeId,
      pageSize: 500,
      offset: 0
    }

    let response = await this.dataService.fetchSessions(fetchSessionsObj);

    if (response.metadata.status == 'SUCCESS') {
      this.sessions = response.payload;
    }
   
  }

}
