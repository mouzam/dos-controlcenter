import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { NgForm } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService} from '../../Services/message.service';
import { AuthService } from 'src/app/Services/auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  signUpForm: FormGroup;
  isSubmitted  =  false;
  messageObj = { };

  constructor( private router: Router, public auth: AuthService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    // Build Form on Component Initialization
    this.signUpForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }
 // On Signup Method
 Signup() {
  this.isSubmitted = true;

  if (this.signUpForm.invalid) {
    return;
  } else if (this.signUpForm.valid) {
    this.isSubmitted = false;
    this.auth.signup(this.signUpForm.value);
    this.signUpForm.reset();
  }
 }
}
