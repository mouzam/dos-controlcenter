import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';

import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(private auth: AuthService, private router: Router) {
  }
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // Check to see if a user has a valid token
    if (this.auth.isAuthenticated()) {
      // If they do, return true and allow the user to load app

      return true;
    }

    // If not, redirect to the login page
    this.router.navigate(['sign-in'], {
      queryParams: {
        return : state.url,
      }
    });
    return false;
  }
}
