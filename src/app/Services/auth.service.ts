import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {Constants} from '../Utilities/constants';
import {Sessions} from '../Utilities/session';
import {DataService} from './data.service';
import {StorageService} from './storage.service'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor( public router: Router,public dataService: DataService, public storageService: StorageService) { }

  public displayError: boolean;
  public displaySignUpMessage: boolean;
  loginMessage;
  signUpMessage;

  loginObj = {
    email: Constants.localStorageAuth.email ,
    password: Constants.localStorageAuth.password
  };

  // Login with email & password
  async login(email: string, password: string) {

    let loginObj= {
      username: email,
      password: password
    }
    let response = await this.dataService.login(loginObj)


    console.log("Login :  ", response);

    if (response.metadata.status == "SUCCESS") {

      Sessions.createLocal( Constants.localStorageObjects.merchantInfo, response.payload);
      //Sessions.createLocal( Constants.localStorageObjects.storeId, response.payload.store.storeId);
      //Sessions.createLocal( Constants.localStorageObjects.branches, response.payload.store.branches);

      this.router.navigate(['merchantDetails']);
    }else {
      this.displayError = true;
      this.loginMessage = Constants.ResponseMessages.loginError;
    }

    this.authenticateUser(email, password);
    setTimeout(() => (this.displayError = false), 3000);
  }
   // Sign up with email and password
   signup(value) {

     this.displaySignUpMessage = true;
     this.signUpMessage = Constants.ResponseMessages.signupSuccess;
     console.log (value);
     setTimeout(() => (this.displaySignUpMessage = false), 3000);
  }

  authenticateUser(username: string, password: string) {

    // set the token in localstorage
    localStorage.setItem('access_token', 'jwt_token');
  }

  isAuthenticated() {
    // get the auth token from localStorage
    const token = localStorage.getItem('access_token');

    // check if token is set, then...
    if (token) {
        return true;
    }
    return false;
  }
  logout() {
    this.router.navigate(['sign-in']);
    // remove the localstorage token
    localStorage.removeItem('access_token');
  }
}
