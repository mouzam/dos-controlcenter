import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Constants } from '../Utilities/constants';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private httpService: HttpService) { }

  async login(loginObj) {

    console.log("login obj =", loginObj)

    let loginUrl = Constants.baseUrl + Constants.login;

    let apiResponse = await this.httpService.post(loginUrl, loginObj).toPromise();
    return (apiResponse.json());
  }

  
  async fetchMerchantDetails() {
    let merchantDetailsUrl = Constants.baseUrl + Constants.merchantDetails;

    let apiResponse = await this.httpService.get(merchantDetailsUrl).toPromise();
    return (apiResponse.json());
  }

  //fetchMerchantInformation
  
  
  async fetchMerchantInformation(storeObj) {
    let merchantInformationUrl = Constants.baseUrl + Constants.merchantInformation;

    let apiResponse = await this.httpService.post(merchantInformationUrl , storeObj) .toPromise();
    return (apiResponse.json());
  }


  async createMerchant(createMerchantObj) {
    let createMerchantUrl = Constants.baseUrl + Constants.createMerchant;

    let apiResponse = await this.httpService.post(createMerchantUrl, createMerchantObj).toPromise();
    return (apiResponse.json());
  }

  
  async createBranch(createBranchObj) {
    let createBranchUrl = Constants.baseUrl + Constants.createBranch;

    let apiResponse = await this.httpService.post(createBranchUrl, createBranchObj).toPromise();
    return (apiResponse.json());
  }


//
  async fetchOffers(fetchOffersObj) {
    let fetchoffersUrl = Constants.baseUrl + Constants.fetchOffers;

    let apiResponse = await this.httpService.post(fetchoffersUrl, fetchOffersObj).toPromise();
    return (apiResponse.json());
  }

  async createOffers(createOffersObj) {
    let createOffersUrl = Constants.baseUrl + Constants.createOffer;

    let apiResponse = await this.httpService.post(createOffersUrl, createOffersObj).toPromise();
    return (apiResponse.json());
  }


  async updateOffers(updateOffersObj) {
    let updateOffersUrl = Constants.baseUrl + Constants.updateOffer;

    let apiResponse = await this.httpService.post(updateOffersUrl, updateOffersObj).toPromise();
    return (apiResponse.json());
  }

  async fetchSessions(fetchSessionsObj) {
    let fetchoffersUrl = Constants.baseUrl + Constants.fetchSession;

    let apiResponse = await this.httpService.post(fetchoffersUrl, fetchSessionsObj).toPromise();
    return (apiResponse.json());
  }

  async getAllUsers() {

    let getAllUsers = Constants.baseUrl + Constants.allUsers;

    let apiResponse = await this.httpService.get(getAllUsers).toPromise();
    return (apiResponse.json());
  }
}
