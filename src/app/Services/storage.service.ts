import { Injectable } from '@angular/core';
import { Subject, Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  private storageSub= new Subject<boolean>();


  watchStorage(): Observable<any> {
   // console.log("Watch Storage called", this.storageSub)
    return this.storageSub.asObservable();
  }

  // set key value in local storage
  setItem(key: string, data: any) {
    localStorage.setItem(key, JSON.stringify(data));
    this.storageSub.next(true);

  }
  removeItem(key) {
    localStorage.removeItem(key);
    this.storageSub.next(true);
  }
}
