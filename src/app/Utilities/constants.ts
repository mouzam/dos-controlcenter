import { environment } from '../../environments/environment';

export class Constants {

  constructor() {
  }

  //http://localhost:3000

  public static baseUrl = 'http://3.1.104.232/dos';

  public static login = '/api/v1/merchant/login';

  public static merchantDetails = '/api/admin/v1/merchant/fetch';
  public static createMerchant = '/api/admin/v1/merchant/register';
  public static createBranch = '/api/admin/v1/branch/add';
  public static fetchOffers = '/api/v1/fetch/offers';
  public static createOffer = '/api/v1/add/offer';
  public static updateOffer = '/api/v1/update/offer';

  public static merchantInformation = '/api/admin/v1/merchant/detail';

  public static fetchSessions = '/api/v1/session/fetch';
  public static fetchSession = '/api/v1/session/fetch';

  public static fetchCustomers = '/api/v1/customer/fetch';

  public static allUsers = '/allUsers';

  public static localStorageAuth = {
    email: 'admin@dos.com',
    password: 'admin'
  };

  public static localStorageObjects = {
    login: 'sessiontoken',
    merchantInfo: 'merchantInfo',
    merchantStoreId: 'merchantStoreId',
    storeId: 'storeid',
    branches: 'branches',
    fetchMerchantDetails: 'merchantDetails',
    merchantInformation: 'merchantInformation',
    merchantBranches: 'merchantBranches',
    merchantId: 'merchantId'
  };
  public static defaults = {
    pageSize: 500,
    offset: 0
  };

  public static displayColorCode = {
    color: '#33FFE5'
  };

  public static ResponseMessages = {

    notFound: 'No Matches Found',
    exceptionFind: 'Error occur. No Matches Found',
    exception: 'Error occur. Form Not Submitted',

    submitMessage: 'Submitted Successfully!',
    notSubmitMessage: 'Form Not Submitted. Try Again!',

    loginSuccess: 'User Logged In Successfully',
    loginError: 'Invalid email or Password',
    signupSuccess: 'Account Created Successfully',
    signupError: 'Error in Creating User',
  };
}
