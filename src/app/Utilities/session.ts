import { Subject, Observable } from "rxjs";
import { Injectable } from "@angular/core";
@Injectable()
export class Sessions {

    constructor() { }
  
    private static storageSub= new Subject<boolean>();

    public static watchStorage(): Observable<any> {
      return this.storageSub.asObservable();
    }
  
    public static createLocal(key, object){
        localStorage.setItem(key, JSON.stringify(object));
        this.storageSub.next(true);
    }
    
    public static createLocalString(key, object){
        localStorage.setItem(key, object);
    }
     
    public static getLocal(key){
        let localData =  localStorage.getItem(key);
        return JSON.parse(localData);
    }

    public static deleteLocal(key){
        localStorage.removeItem(key)
        
    }
}