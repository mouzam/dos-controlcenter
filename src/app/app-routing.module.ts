import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {LoginComponent} from './Components/login/login.component';
import {SignUpComponent} from './Components/sign-up/sign-up.component';
import {NavbarComponent} from './Components/navbar/navbar.component';
import {DashboardComponent} from './Components/dashboard/dashboard.component';

import {HomeComponent} from './Components/home/home.component';
import {MerchantDetailsComponent} from './Components/Merchant/merchant-details/merchant-details.component';
import {AddMerchantComponent} from './Components/Merchant/add-merchant/add-merchant.component';
import {AddBranchComponent} from './Components/Merchant/add-branch/add-branch.component';

import {MerchantInformationComponent} from './Components/Merchant/merchant-information/merchant-information.component';

import {OffersComponent} from './Components/offers/offers.component';
import {SessionsComponent} from './Components/sessions/sessions.component';
import {RechargeComponent} from './Components/recharge/recharge.component';
import {PaymentComponent} from './Components/payment/payment.component';
import {HelpComponent} from './Components/help/help.component';
import {SupportComponent} from './Components/support/support.component';


import {AuthGuardService} from './Services/auth-guard.service';
const routes: Routes = [
  { path: '', redirectTo: 'merchantDetails', pathMatch: 'full' },
  {
    path: 'sign-in',
    component: LoginComponent
  },
  {
    path: 'sign-up',
    component: SignUpComponent
  },
  {
    path: 'navbar',
    component: NavbarComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'merchantDetails',
    component: MerchantDetailsComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'addMerchant',
    component: AddMerchantComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'addBranch',
    component: AddBranchComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'merchant-information',
    component: MerchantInformationComponent,
    canActivate: [AuthGuardService]
  },
  
  {
    path: 'offers',
    component: OffersComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'sessions',
    component: SessionsComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'recharge',
    component: RechargeComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'payment',
    component: PaymentComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'help',
    component: HelpComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'support',
    component: SupportComponent,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
