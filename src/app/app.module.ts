import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';

import { NgModule } from '@angular/core';
import { HttpModule} from '@angular/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatFormFieldModule} from '@angular/material/form-field';


import { AppRoutingModule } from './app-routing.module';
import { HttpService} from './Services/http.service';
import { AppComponent } from './app.component';
import { NavbarComponent } from './Components/navbar/navbar.component';
import { LoginComponent } from './Components/login/login.component';
import { BannerComponent } from './Components/banner/banner.component';
import { DashboardComponent } from './Components/dashboard/dashboard.component';
import { SignUpComponent } from './Components/sign-up/sign-up.component';

import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { OffersComponent } from './Components/offers/offers.component';
import { SessionsComponent } from './Components/sessions/sessions.component';
import { RechargeComponent } from './Components/recharge/recharge.component';
import { PaymentComponent } from './Components/payment/payment.component';
import { HelpComponent } from './Components/help/help.component';
import { SupportComponent } from './Components/support/support.component';
import { HomeComponent } from './Components/home/home.component';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { MerchantDetailsComponent } from './Components/Merchant/merchant-details/merchant-details.component';
import { AddMerchantComponent } from './Components/Merchant/add-merchant/add-merchant.component';
import { AddBranchComponent } from './Components/Merchant/add-branch/add-branch.component';
import { MerchantInformationComponent } from './Components/Merchant/merchant-information/merchant-information.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    BannerComponent,
    DashboardComponent,
    SignUpComponent,
    OffersComponent,
    SessionsComponent,
    RechargeComponent,
    PaymentComponent,
    HelpComponent,
    SupportComponent,
    HomeComponent,
    MerchantDetailsComponent,
    AddMerchantComponent,
    AddBranchComponent,
    MerchantInformationComponent,
    //NgbModalBackdrop
  ],
  imports: [
    NgbModule,
    MDBBootstrapModule.forRoot(),
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    AngularFontAwesomeModule,
    MatFormFieldModule 
    //MatInputModule
  ],
   entryComponents: [
     //NgbModalBackdrop
  ],
  providers: [HttpService],
  bootstrap: [AppComponent]
})
export class AppModule { }
